import React from 'react'
import './App.css'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Home from './Components/Home'
import Product from './Components/Product'
import Shoepage from './Components/Shoepage'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
         <Route exact path="/" component={Shoepage} />
         <Route exact path="/login" component={Home} />
         <Route exact path="/product" component={Product} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
